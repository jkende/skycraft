import { Box, Flex, Text, Center } from "@chakra-ui/react"

export default function Footer() {
    return (
        <>
            <Box
                p="0"                
                alignItems="center"
                justifyContent="center"
                textAlign="center"
                backgroundColor="black"
                width="100%"
                height="xs"
                color="white"
            >
                <Center backgroundColor="black" pb={100}>            
                    <Flex align="center">
                        <Text fontSize="xs" fontWeight="thin" color="white" pt="150px">(replace with footer)</Text>
                    </Flex>
                </Center>
            </Box>
        </>
    );
}