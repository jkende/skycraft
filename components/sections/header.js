import React from 'react'
import Link from 'next/link'
import { Box, Flex, Text, Button } from "@chakra-ui/react"
import { CgMenuGridO, CgCloseO } from 'react-icons/cg'
import { ImNewTab } from 'react-icons/im'
// import SvgHeaderLogo from '../ui/svg/SvgHeaderLogo'

const MenuItems = props => {
    const { children, isLast, to = "/", ...rest } = props
    return (
        <Text
            mb={{ base: isLast ? 0 : 8, sm: 0 }}
            mr={{ base: 0, sm: isLast ? 0 : 8 }}
            display="block"
            {...rest}
        >
            <Link href={to}>
                <a>{children}</a>
            </Link>
        </Text>
    )
}

const Header = props => {
    const [show, setShow] = React.useState(false)
    const toggleMenu = () => setShow(!show)

    return (
        <Flex
            as="nav"
            align="center"
            justify="space-between"
            wrap="wrap"
            w="100%"
            mb={8}
            paddingY={8}
            paddingX={[8, 8, 0, 0]}
            bg="transparent"
            color="primary.700"
            {...props}
        >
            <Flex align="center">
                {/* <Box as={SvgHeaderLogo} /> */}
                <Text fontSize="2xl" fontWeight="bold" color="white">
                    <Link href="/">SKYCRAFT</Link>                    
                </Text>
            </Flex>

            <Box
                display={{ sm: show ? "block" : "none", md: "block" }}
                flexBasis={{ base: "100%", md: "auto" }}
            >
                <Flex
                    align={["center", "center", "center", "center"]}
                    justify={["center", "space-between", "flex-end", "flex-end"]}
                    direction={["column", "row", "row", "row"]}
                    pt={[4, 4, 0, 0]}
                    color="white"
                >
                    
                    <MenuItems to="/explore" pr="30px">Explore</MenuItems>
                    <MenuItems to="/stake" pr="30px">Stake</MenuItems>
                    <MenuItems to="/dividend" pr="30px">Dividend</MenuItems>
                    <MenuItems to="/about" pr="30px">About</MenuItems>
                                       
                    <MenuItems to="/create" isLast>                            
                        <Button
                            size="sm"
                            rounded="md"
                            color="white"
                            bg="black"
                            border="2px"
                            _hover={{
                                bg: "white",
                                border: "2px",
                                color: "black",
                            }}
                            rightIcon={<ImNewTab />}
                        >
                            Create
                        </Button>
                    </MenuItems>                   
                </Flex>
            </Box>

            <Box display={{ sm: "block", md: "none" }} onClick={toggleMenu}>
                {show ? <CgCloseO color="white" /> : <CgMenuGridO color="white" />}
            </Box>
            
        </Flex>
    )
}

export default Header