import { Box, Flex, Text } from "@chakra-ui/react"

export default function Ticker() {
    return (
        <>
            <Box
                p="0"
                alignItems="center"
                justifyContent="center"
                textAlign="center"
                backgroundColor="#3629FA"
                width="100%"
                height="60px"
                color="white"
            >
            <Flex align="center">
                <Text fontSize="xs" fontWeight="thin" color="white" pt="20px" pl="20px">
                    (replace with ticker)
                </Text>
            </Flex>
            </Box>
        </>
    );
}