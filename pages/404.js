import { Box, Center, Text } from '@chakra-ui/react'

export default function Custom404() {
    return (
        <Box backgroundColor="black" color="white" height="80vh">
            <Center height="50vh">
                <Text fontSize="xl" color="white" pt="150px">
                    <h1>404 - These are not the NFTs you're looking for.</h1>
                </Text>
            </Center>
        </Box>
    )
  }