import { ChakraProvider, Box } from "@chakra-ui/react"
import theme from '@/design-system'

function MyApp({ Component, pageProps }) {
  return (
    <>      
      <ChakraProvider resetCSS theme={theme} portalConfig={{ zIndex: 40 }}>  
        <Box backgroundColor="black">
          <Component {...pageProps} />
        </Box>
      </ChakraProvider>      
    </>
  );
}

export default MyApp
