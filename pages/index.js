import Head from 'next/head'
import {
  Box,
  Image,
  Grid,
  Flex,
  Text
} from '@chakra-ui/react'


export default function Home() {
  return (
    <>
      <Head>
          <title>🚀</title>
          <link rel="icon" href="/favicon.ico" />
      </Head>
      <Flex
          display="flex"
          flexDirection="column"
          alignItems="center"
          justifyContent="center"
          textAlign="center"
          mt={0}
          backgroundColor="black"
      >
        {/* <LandingHero /> */}
        <Flex
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
            textAlign="center" 
            position="relative"           
            _before={{
                background: "black",
                content: '""',                           
                backgroundSize: "cover",
                position: "absolute",
                top: "0",
                right: "0",
                bottom: "0",
                left: "0",
                // bgImage: "url('/assets/cryptomancy-hero.png')",
                bgImage: "url('/assets/dragon-4907812_1280.png')",
                bgPos: "center",
                bgRepeat: "no-repeat",
                bgSize: "36%",
                opacity: "0.56"        
            }}
            width="100%"
            height="4xl"
            mt={0}            
        >
          <Text fontSize="6xl" fontWeight="bold" opacity="1.0" px={[30, 30, 60, 120]} pb={[60, 60, 120, 240]} color="white">
            
          </Text>
        </Flex>
      </Flex>      
    </>
  );
}
